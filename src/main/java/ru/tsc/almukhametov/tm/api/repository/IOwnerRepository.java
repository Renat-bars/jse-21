package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    E add(String userId, E entity);

    void remove(String userId, E entity);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    void clear(String userId);

    Optional<E> findById(String userId, final String id);

    Optional<E> findByIndex(String userId, final Integer index);

    Optional<E> removeById(String userId, final String id);

    Optional<E> removeByIndex(String userId, final Integer index);

    boolean existById(String userId, final String id);

    boolean existByIndex(String userId, final int index);

}
