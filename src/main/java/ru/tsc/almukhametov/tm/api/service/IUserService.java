package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.api.IService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    Optional<User> findByLogin(String login);

    boolean isLoginExists(String login);

    Optional<User> findByEmail(String email);

    boolean isEmailExists(String email);

    Optional<User> removeUser(User user);

    Optional<User> removeUserById(String id);

    Optional<User> removeUserByLogin(String login);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User setPassword(String userId, String password);

    User setRole(String userId, Role role);

}