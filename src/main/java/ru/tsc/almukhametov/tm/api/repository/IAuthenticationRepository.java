package ru.tsc.almukhametov.tm.api.repository;

public interface IAuthenticationRepository {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

}
