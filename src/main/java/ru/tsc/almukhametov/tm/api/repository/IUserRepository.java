package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    Optional<User> removeUser(User user);

    Optional<User> removeUserById(String id);

    Optional<User> removeUserByLogin(String login);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User setPassword(String userId, String password);

}