package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IAuthenticationService {

    Optional<User> getUser();

    String getUserId();

    boolean isAuthentication();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
