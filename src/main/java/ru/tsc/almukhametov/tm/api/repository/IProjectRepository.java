package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Optional<Project> findByName(String userId, String name);

    Optional<Project> removeByName(String userId, String name);

    Project startById(String userId, String id);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project finishById(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

}
