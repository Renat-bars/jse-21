package ru.tsc.almukhametov.tm.api;

import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    Optional<E> remove(E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void clear();

    Optional<E> findById(final String id);

    Optional<E> findByIndex(final Integer index);

    Optional<E> removeById(final String id);

    Optional<E> removeByIndex(final Integer index);

    boolean existById(final String id);

    boolean existByIndex(final int index);

    Integer getSize();

}
