package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_SHOW_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_SHOW_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Optional<Task> task = serviceLocator.getTaskService().findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        showTasks(task.get());
    }

}
