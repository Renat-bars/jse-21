package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return TerminalConst.TASK_REMOVE_BY_NAME;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_REMOVE_BY_NAME;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        final Optional<Task> task = serviceLocator.getTaskService().removeByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
    }

}
