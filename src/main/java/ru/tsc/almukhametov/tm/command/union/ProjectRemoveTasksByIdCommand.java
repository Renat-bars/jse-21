package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectRemoveTasksByIdCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Optional<Project> project = serviceLocator.getProjectTaskService().removeById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
    }

}
