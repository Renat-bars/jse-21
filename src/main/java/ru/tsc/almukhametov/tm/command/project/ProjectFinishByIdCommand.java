package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_FINISH_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_FINISH_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        else project.setFinishDate(new Date());
    }

}
