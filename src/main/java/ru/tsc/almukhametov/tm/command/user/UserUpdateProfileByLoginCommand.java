package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserUpdateProfileByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_BY_LOGIN;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_UPDATE_BY_LOGIN;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        final Optional<User> user = serviceLocator.getUserService().findByLogin(login);
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(user.get().getId(), firstName, lastName, middleName);
    }
}
