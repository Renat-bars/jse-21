package ru.tsc.almukhametov.tm.command.task;

import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_REMOVE_BY_INDEX;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_REMOVE_BY_INDEX;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Optional<Task> task = serviceLocator.getTaskService().removeByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
    }

}
