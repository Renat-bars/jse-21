package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Date;
import java.util.Optional;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_START_BY_ID;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_START_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Optional<Project> project = serviceLocator.getProjectService().startById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        else project.ifPresent(p -> p.setStartDate(new Date()));
    }

}
