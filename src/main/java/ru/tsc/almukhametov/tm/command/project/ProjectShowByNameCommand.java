package ru.tsc.almukhametov.tm.command.project;

import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return TerminalConst.PROJECT_SHOW_BY_NAME;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_SHOW_BY_NAME;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        final Optional<Project> project = serviceLocator.getProjectService().findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        showProject(project.get());
    }

}
