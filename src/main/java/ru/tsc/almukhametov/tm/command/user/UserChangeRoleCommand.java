package ru.tsc.almukhametov.tm.command.user;

import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class UserChangeRoleCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return TerminalConst.USER_CHANGE_ROLE;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.USER_CHANGE_ROLE;
    }

    @Override
    public void execute() {
        System.out.println("CHANGE USER ROLE");
        System.out.println("ENTER USER ID");
        final String userId = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE:");
        final String roleString = TerminalUtil.nextLine();
        if (roleString == Role.USER.getDisplayName() || Role.ADMIN.getDisplayName() == roleString)
            serviceLocator.getUserService().setRole(userId, Role.USER);
    }

}
