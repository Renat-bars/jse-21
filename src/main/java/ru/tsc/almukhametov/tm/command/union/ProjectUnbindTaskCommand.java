package ru.tsc.almukhametov.tm.command.union;

import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class ProjectUnbindTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return TerminalConst.TASK_UNBIND_FROM_PROJECT;
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return SystemDescriptionConst.TASK_UNBIND_FROM_PROJECT;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("[Unbind task to project]");
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskId == null) throw new TaskNotFoundException();
        System.out.println("Enter project Id");
        final String projectId = TerminalUtil.nextLine();
        if (projectId == null) throw new ProjectNotFoundException();
        final Task task = serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

}
