package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IUserRepository;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    protected final List<User> list = new ArrayList<>();

    protected Predicate<User> predicateByLogin(final String login) {
        return u -> login.equals(u.getLogin());
    }

    protected Predicate<User> predicateByEmail(final String email) {
        return u -> email.equals(u.getEmail());
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return list.stream()
                .filter(predicateByLogin(login))
                .findFirst();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return list.stream()
                .filter(predicateByEmail(email))
                .findFirst();
    }

    @Override
    public Optional<User> removeUser(final User user) {
        list.remove(user);
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> removeUserById(final String id) {
        final Optional<User> user = findById(id);
        list.remove(user);
        return user;
    }

    @Override
    public Optional<User> removeUserByLogin(final String login) {
        final Optional<User> user = findByLogin(login);
        list.remove(user);
        return user;
    }

    @Override
    public User updateUser(final String userId, final String firstName, final String lastName, final String middleName) {
        final Optional<User> user = findById(userId);
        user.ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setMiddleName(middleName);
        });
        return user.orElse(null);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        final Optional<User> user = findById(userId);
        user.ifPresent(u -> {
            u.setPasswordHash(HashUtil.salt(password));
        });
        return user.orElse(null);
    }

}