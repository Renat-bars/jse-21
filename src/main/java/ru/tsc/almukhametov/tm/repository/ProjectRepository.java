package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Optional;
import java.util.function.Predicate;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    protected Predicate<Project> predicateByName(final String name) {
        return p -> name.equals(p.getName());
    }

    @Override
    public Optional<Project> findByName(final String userId, final String name) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByName(name))
                .findFirst();
    }

    public Optional<Project> removeByName(final String userId, final String name) {
        final Optional<Project> project = (findByName(userId, name));
        project.ifPresent(this::remove);
        return project;
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @Override
    public Project finishById(final String userId, String id) {
        final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public Project changeProjectStatusById(final String userId, String id, Status status) {
        final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(status));
        return project.get();
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, Integer index, Status status) {
        final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(status));
        return project.get();
    }

    @Override
    public Project changeProjectStatusByName(final String userId, String name, Status status) {
        final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(status));
        return project.get();
    }

}
