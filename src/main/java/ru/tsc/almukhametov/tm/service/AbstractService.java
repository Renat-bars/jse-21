package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.api.IService;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(entity);
    }

    @Override
    public Optional<E> remove(E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
        return Optional.empty();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public Optional<E> findById(final String id) {
        return repository.findById(id);
    }

    @Override
    public Optional<E> findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.findByIndex(index);
    }

    @Override
    public Optional<E> removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public Optional<E> removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.removeByIndex(index);
    }


    @Override
    public boolean existById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existById(id);
    }

    @Override
    public boolean existByIndex(final int index) {
        return repository.existByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
