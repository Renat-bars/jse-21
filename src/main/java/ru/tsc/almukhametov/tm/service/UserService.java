package ru.tsc.almukhametov.tm.service;

import ru.tsc.almukhametov.tm.api.repository.IUserRepository;
import ru.tsc.almukhametov.tm.api.service.IUserService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.*;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.exception.system.EmailExistsException;
import ru.tsc.almukhametov.tm.exception.system.LoginExistsException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public Optional<User> findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException(login);
        if (isEmailExists(email)) throw new EmailExistsException(email);
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        return userRepository.findByLogin(login).isPresent();
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Override
    public Optional<User> removeUser(final User user) {
        if (user == null) return Optional.empty();
        return userRepository.removeUser(user);
    }

    @Override
    public Optional<User> removeUserById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @Override
    public Optional<User> removeUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeUserByLogin(login);
    }

    @Override
    public User updateUser(final String userId, final String firstName, String lastName, String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        return user.get();
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.get().setPasswordHash(HashUtil.salt(password));
        return user.get();
    }

    @Override
    public User setRole(final String userId, final Role role) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.get().setRole(role);
        return user.get();
    }

}