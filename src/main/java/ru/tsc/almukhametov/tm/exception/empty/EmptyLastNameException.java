package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyLastNameException extends AbstractException {

    public EmptyLastNameException() {
        super("Error! Last name is empty");
    }

}
