package ru.tsc.almukhametov.tm.exception.system;

import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException(final String login) {
        super("Incorrect argument. Argument ``" + login + "`` was not founded. " +
                "Use ``" + ArgumentConst.HELP + "`` for display list of arguments");
    }
}
