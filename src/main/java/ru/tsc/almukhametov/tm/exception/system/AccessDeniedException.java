package ru.tsc.almukhametov.tm.exception.system;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}
